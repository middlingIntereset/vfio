#!/bin/sh

if [ -z "$(ip addr | grep vmtap0)" ];
then
	echo No vmtap0 interface, creating...

	# No vmtap0 yet.
	ip tuntap add mode tap vmtap0
	sleep 0.5s
else
	echo Using existing vmtap0 interface...
fi

# Add to br0
if [ -z "$(brctl show br0 | grep vmtap0)" ];
then
	echo Adding vmtap0 to bridge...
	brctl addif br0 vmtap0
	sleep 0.5s
fi

# Up vmtap0
echo Upping vmtap0 link...
ip link set vmtap0 up
sleep 0.5s

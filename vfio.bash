#!/usr/bin/env bash

vmname="society"
usb_addr="0000:00:14.0" 
usb_vendor_device="0x8086 0x8cb1"

echo "$usb_addr" | sudo tee /sys/bus/pci/devices/"$usb_addr"/driver/unbind
echo "$usb_vendor_device" | sudo tee /sys/bus/pci/drivers/vfio-pci/new_id
cp /usr/share/OVMF/OVMF_VARS.fd /tmp/tmp_vars.fd

sudo qemu-system-x86_64 \
    -nodefaults \
    -enable-kvm \
    -name $vmname,process=$vmname \
    -machine type=q35,accel=kvm \
    -smp 6,cores=3,threads=2,sockets=1 \
    -cpu host,kvm=off,check \
    -m 16G \
    -mem-prealloc \
    -rtc clock=host,base=localtime \
    -serial none \
    -parallel none \
    -device vfio-pci,host=02:00.0,multifunction=on,x-vga=on \
    -device vfio-pci,host=02:00.1 \
    -device vfio-pci,host=00:14.0 \
    -drive if=pflash,format=raw,readonly,file=/usr/share/OVMF/OVMF_CODE.fd \
    -drive if=pflash,format=raw,file=/tmp/tmp_vars.fd \
    -nographic \
    -vga none \
    -monitor stdio \
    -boot order=c,menu=on \
    -device ide-cd,bus=ide.0,drive=install-cd \
    -drive file=/home/cicero/Downloads/Win10_1809Oct_v2_English_x64.iso,id=install-cd,if=none,format=raw \
    -device ide-cd,bus=ide.1,drive=driver-cd \
    -drive file=/home/cicero/Downloads/virtio-win-0.1.141.iso,id=driver-cd,if=none,format=raw \
    -object iothread,id=iothread1 \
    -device driver=virtio-scsi-pci,id=scsi1,iothread=iothread1 \
    -device scsi-hd,bus=scsi1.0,drive=hd1 \
    -drive if=none,id=hd1,file=/media/cicero/Vegas/Vegas/win.img,format=raw,aio=threads \
    -netdev user,hostfwd=tcp::24800-:24800,hostname=forward,id=net \
    -net nic,netdev=net
    
    
